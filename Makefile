##
# Template Makefile
#
# @file
# @version 0.1

# Borrowed from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
# Via: https://news.ycombinator.com/item?id=21812897
#
# More: https://tech.davis-hansson.com/p/make/

.PHONY: help
help: ## Display this help section
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-38s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
.DEFAULT_GOAL := help

.PHONY: do-something
do-something: ## Do some stuff
	echo "Hello world"
	echo "How you doin?"

# end
